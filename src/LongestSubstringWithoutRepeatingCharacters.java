import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class LongestSubstringWithoutRepeatingCharacters {
	
	static  Map<String,Integer> resultmap=new LinkedHashMap();
	public static void main(String[] args) {
		String s="";
		System.out.println(getSubsets(s));
		for(String ss:getSubsets(s)) {
			getMap(ss);
		}
		System.out.println(resultmap);
		Map<String,Integer> newMap=resultmap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,(e1,e2)->e1, LinkedHashMap::new));
		for(String ss:newMap.keySet()) {
			System.out.println(ss);
			break;
		}

	}

	
	public static Set<String> getSubsets(String s){
		Set<String> sub=new TreeSet<>();
		
		for(int i=0;i<s.length();i++) {
			for(int j=i;j<s.length();j++) {
				sub.add(s.substring(i,j+1));
			}
		}
		return sub;
		
		
	}
	
	public static void getMap(String s){
		
		Map<Character,Integer> map=new HashMap<Character, Integer>();
		for(int i=0;i<s.length();i++) {
			if(map.containsKey(s.charAt(i))) {
				map.put(s.charAt(i), map.get(s.charAt(i))+1);
			}else {
				map.put(s.charAt(i), 1);
			}
		}
		int count=0;
		for(Character c:map.keySet()) {
			if(map.get(c)>1) {
				count++;
				break;
			}
		}
		if(count==0) {
			resultmap.put(s,s.length());
		}
		
		
	}
}
