import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CountMaxOnes {
	
	
	public static void main(String[] args) {
	
		int[][] a= {{1,1,1,1},{1,1,1,1},{1,1,1,1},{0,0,0,0},{0,0,1,1}};
		System.out.println(a.length);
		int max=0;
		int maxrow=0;
		List<List<Integer>> ans=new ArrayList<List<Integer>>();
		for(int i=0;i<5;i++) {
			List<Integer> rows=new ArrayList<>();
			int count=0;
			for(int j=0;j<4;j++) {
				if(a[i][j]==1) {
					count++;
				}
			}
			
		if(max<count) {
			max=count;
			maxrow=i;
		}
	}
	System.out.println(maxrow);
	
	}
}


//Test cases:-
//1.Test if matrix is not empty/null
//2.Check if all the row contains the same no. of 1s
//3.Check if none of the row contains 1s just contains 0s
//4.Test if 2 rows have same max count
