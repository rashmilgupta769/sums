import java.util.Arrays;

public class MergeWithoutExtraSpace {
	
	public static void main(String[] args) {
		int n = 4;
				int m = 5;
				int arr1[] = {1, 3, 5, 7};
				int arr2[] = {0, 2, 6, 8, 9};
				merge(arr1, arr2, n, m);
	}

	 

	    public static void merge(int arr1[], int arr2[], int n, int m) {
	    	int i=0;
	      Arrays.sort(arr1);
	      Arrays.sort(arr2);
	      while(i<n && i<m) {
	    	  if(arr1[i]>arr2[i]) {
	    		 int temp=0;
	    		 temp=arr1[i];
	    		 arr1[i]=arr2[i];
	    		 arr2[i]=temp;
	    	  }
	    	  i++;
	      }
	      System.out.println(arr1);

}
}