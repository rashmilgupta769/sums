import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnagramsSol {

	public static void main(String[] args) {
		String[] string_list = {"bfj", "tro", "ffa", "rph"};// TODO Auto-generated method stub
Anagrams(string_list);
	}
	public static List<List<String>> Anagrams(String[] string_list) {
	List<List<String>> result=new ArrayList<List<String>>();
	List<Integer> count=new ArrayList<Integer>();
	for(int i=0;i<string_list.length;i++) {
		List<String> list=new ArrayList<String>();
		for(int j=i+1;j<string_list.length;j++) {
			char[] left=string_list[i].toCharArray();
			Arrays.sort(left);
			char[] right=string_list[j].toCharArray();
			Arrays.sort(right);
			if(new String(left).equals(new String(right))) {
				if(!count.contains(i)) {
					list.add(string_list[i]);
					count.add(i);
				}
				if(!count.contains(j)) {
					list.add(string_list[j]);
					count.add(j);
				}
			}
		}
		if(list.size()>0) {
		result.add(list);
		}
	}
	if(count.size()<string_list.length) {
		for(int i=0;i<string_list.length;i++) {
			if(!count.contains(i)) {
				List<String> l=new ArrayList<String>();
				l.add(string_list[i]);
				result.add(l);
			}
		}
	}
	System.out.println(result);
       return result;
    }
}
