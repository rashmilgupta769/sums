
public class ReverseCharArrayWoSpace {

	public static void main(String[] args) {
		char[]a={'A' , ' ', 'B', ' ', 'C', 'D', ' ', 'E', 'F'};
		
		int j=a.length-1;
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<a.length;i++) {
			if(a[i]>='A' && a[i]<='Z') {
				while(!(a[j]>='A' && a[j]<='Z')) {
					j=j-1;
				}
				sb.append(a[j--]);
			}else {
				sb.append(a[i]);
			}
			
		}
		System.out.println(sb.toString().toCharArray());

	}

}
