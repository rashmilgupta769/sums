import java.util.Scanner;

public class Print1To10WithoutLoop {
	static int i=1;
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a no.");
		int n=sc.nextInt();
		print(n);
		

	}
	
	public static void print (int n) {
		
		if(i<=n) {
			System.out.println(i);
			i++;
			print(n--);
		}
	}
	

}
