
public class Pattern3 {

	public static void main(String[] args) {
		int i, j, row = 6;
		
		for (i = 0; i < row; i++) {
			for (j = 0; j <= row - i; j++) {
				System.out.print(" ");
			}
			for (int k = 0; k <= i; k++) {
				System.out.print("* ");
			}
			System.out.println();

		}

	}

}
