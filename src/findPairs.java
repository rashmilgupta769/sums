import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class findPairs {

	public static void main(String[] args) {
		int arr[] = {3, 4, 7, 1, 2, 9, 8} ;
		
		Map<Integer,Integer> map=new HashMap();
		
		for(int i=0;i<arr.length;i++) {
			for(int j=i+1;j<arr.length;j++) {
				Set<Integer> set=new HashSet<>();
				set.add(arr[i]);
				set.add(arr[j]);
				if(arr[i]!=arr[j]) {
				if(map.containsKey(arr[i]+arr[j])) {
					//System.out.println(set );
					//System.out.println(arr[i]+arr[j]);
					map.put(arr[i]+arr[j], map.get(arr[i]+arr[j])+1);
					
				}else {
					//System.out.println(set );
					//System.out.println(arr[i]+arr[j]);
					map.put(arr[i]+arr[j], 1);
				}
				}
			}
			
		}
		map.entrySet().stream().filter(m -> m.getValue()>1).forEach(System.out::println);
		

	}

}
