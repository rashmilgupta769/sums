import java.util.ArrayList;
import java.util.List;

public class printFirstNegativeIntegerr {

	public static void main(String[] args) {
		long a[] = {12, -1, -7, 8, -15, 30, 16, 28};
		int k=3;
		List<Long> list=new ArrayList<>();
		
		for (int i=0;i<a.length;i++) {
			int size=list.size();
			int j=i;
			if(i+k-1>a.length-1) {
				break;
			}
			while(j<i+k) {
				if(a[j]<0) {
					list.add(a[j]);
					
					break;
				}
				j++;
			}
			if(list.size()==size) {
				list.add((long) 0);
			}
		}
		System.out.println(list.toArray());

	}

}
