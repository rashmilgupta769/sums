import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Test9 {

	public static void main(String[] args) {
		   List<String> stringList = new ArrayList<>();
           stringList.add("ank");
           stringList.add("sam");
           stringList.add("az");
           stringList.add("neh");
           stringList.add("ad");
           
           stringList.stream().filter(s->s.startsWith("a")).sorted(Comparator.reverseOrder()).forEach(System.out::println);
           stringList.stream().filter(s->s.startsWith("a")).sorted().forEach(System.out::println);
           
           stringList.stream().filter(s->s.startsWith("a")).map(String::toUpperCase).sorted().forEach(System.out::println);


	}

}
