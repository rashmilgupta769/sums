import java.util.HashMap;
import java.util.Map;

public class getOddOccurance {

	public static void main(String[] args) {
		int arr[] = {1, 2, 3, 2, 3, 1, 3};
		Map<Integer,Integer> map=new HashMap<>();
		for(int i=0;i<arr.length;i++) {
		
			if(map.containsKey(arr[i])) {
				map.put(arr[i], map.get(arr[i])+1);
			}else {
				map.put(arr[i], 1);
			}
	}
		int res=0;
		for(int a:map.keySet()) {
			if(map.get(a)%2!=0) {
				res=a;
				break;
			}
		}
		System.out.println(res);

	}
}
