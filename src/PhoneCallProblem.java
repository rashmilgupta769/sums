import java.util.HashMap;
import java.util.Map;

public class PhoneCallProblem {

	public static void main(String[] args) {
		String s=	 "00:01:07,400-234-090\n" +
                "00:05:01,701-080-080\n" +
                "00:05:00,400-234-090";
		
		String[] arr=s.split("\n");
		Map<String,Integer> phoneLog=new HashMap<>();
		for(String phone:arr) {
			String ph[]=phone.split(",");
			String phoneNumber=ph[1];
		if(phoneLog.containsKey(phoneNumber)){
			phoneLog.put(phoneNumber, phoneLog.get(phoneNumber)+getSec(ph[0]));
		}else {
			phoneLog.put(phoneNumber, getSec(ph[0]));
		}
		}
		
		int max=0;
		int totelPhoneCal=0;
		for(String ph:phoneLog.keySet()) {
			int val=getCentValue(phoneLog.get(ph));
			System.out.println(val);
			if(max<val) {
				max=val;
			}
			totelPhoneCal=totelPhoneCal+val;
		}
		
		System.out.println(totelPhoneCal);
		System.out.println(totelPhoneCal-max);

	}

	public static int getSec(String date) {
	
		String[] arr=date.split(":");
		return (Integer.parseInt(arr[1])*60)+Integer.parseInt(arr[2]);
		
	}
	
	public static int getCentValue(int num) {
		if(num>300) {
			return ((num/60)+1)*150;
		}else if(num/60<5) {
			return num*3;
		}else {
			return (num/60)*150;
		}
	}
	
}
