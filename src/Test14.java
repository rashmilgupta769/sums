import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test14 {

	public List<String> checkFile(String input,String path) throws IOException{
		
File f=new File(path);
		FileReader fr = null;
		try {
			fr = new FileReader(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader br=new BufferedReader(fr);
		
		return getLines(input, br);
	
	}
private static List<String> getLines(String input,BufferedReader br) throws IOException {
	List<String> nonWordList=new ArrayList<>();
	String line="";
	switch(input) {
	case "Match" :
		while((line=br.readLine())!=null) {
			if(line.matches("Sachin")) {
				
				
				nonWordList.add(line);
				}
						}
		br.close();
		break;
	case "Non-Match":
		while((line=br.readLine())!=null) {
			if(!line.contains("Sachin")) {
				
				
				nonWordList.add(line);
				}
						}
		br.close();
		break;
		}
	return nonWordList;
		
	}
	
}

