package testing;

public class B extends A{
	
	void method(double a)  
    {  
        System.out.println("Base class method called with double a = "+a);  
    } 
	public void inB() {
		System.out.println("In b");
	}
	B() {
	System.out.println("In b ");
	}
	B(int num){
		this();
		System.out.println(num);
	}
	
}
