package testing;

class SuperClass{
    public void sum(int x, int y) {
           System.out.println("sum of 2 int type arguments = "+ (x+y));
    }
}
 
class SubClass extends SuperClass{
    public void sum(int x, int y) {
           System.out.println("sum of 2 double type arguments = "+ (x+y));
    }
}
 
 
/** Copyright (c), AnkitMittal JavaMadeSoEasy.com */
public class MyClass {
 
    public static void main(String[] args) {
    	SuperClass obj = new SubClass();
           obj.sum(2, 1);
           obj.sum(2, 3);
    }
 
}
 
