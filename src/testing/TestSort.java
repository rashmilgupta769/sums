package testing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestSort {
	
	public  void mainst() {
		Student s=new Student(0, null, 0);
	}

	public static void main(String[] args) {
		TestSort ts=new TestSort();
		ts.mainst();
		System.out.println(Student.school);
		int i = 25;
		System.out.println(i++);
		int j = i++;
		System.out.println(i + " " + j);

		   List<Student> stList=new ArrayList<>();
		    stList.add(new Student(1, "rashmil", 27));
		    stList.add(new Student(2, "pushpil", 29));
		    stList.add(new Student(1, "akshay", 18));
		  
		   int age= stList.stream().mapToInt(s->s.age).sum();
		   System.out.println("SUM>>>>>>>>>>>>>");
		   System.out.println(age);
		    System.out.println("Before sort");
		    System.out.println(stList);
		    System.out.println("After sort");
		   System.out.println( stList.stream().sorted(Comparator.comparing(Student::getName)).collect(Collectors.toList()));
	
		   Map<Integer, Student> m=new HashMap<>();
			  m.put(1, new Student(1, "rashmil", 27));
			  m.put(2,new Student(2, "pushpil", 29));
			    m.put(3,new Student(1, "akshay", 18));
			    System.out.println("****************");
			    m.forEach((k,v)->System.out.println(k+" "+v));
			    m.entrySet().forEach(System.out::println);
			    System.out.println("****************");
			    System.out.println("Before sort");
			    System.out.println(m);
			    System.out.println("After sort");
			    Map<Integer, Student> newm=  m.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.comparing(Student::getName).reversed())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(e1,e2)->e1,LinkedHashMap::new));
			    System.out.println(newm);
	}

}
