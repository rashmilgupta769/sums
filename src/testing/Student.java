package testing;

public class Student {
	int rollno;    
	   String name;    
	  int age;   
	  static String school ="a";
	  Student(int rollno,String name,int age){ 
		  Student.school="c";
	    this.rollno=rollno;    
	    this.name=name;    
	    this.age=age;    
	    }  
	  
	    public int getRollno() {  
	        return rollno;  
	    }  
	  
	    public void setRollno(int rollno) {  
	        this.rollno = rollno;  
	    }  
	  
	    public String getName() {  
	        return name;  
	    }  
	  
	    @Override
		public String toString() {
			return "Student [rollno=" + rollno + ", name=" + name + ", age=" + age + "]";
		}

		public void setName(String name) {  
	        this.name = name;  
	    }  
	  
	    public int getAge() {  
	        return age;  
	    }  
	  
	    public void setAge(int age) {  
	        this.age = age;  
	    }  
}
