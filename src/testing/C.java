package testing;

public  class C extends B{
	//public  abstract static void checkAbstractC();
	
	C(){
		System.out.println("hi");
	}
	void method(double a)  
    {  
        System.out.println("derived class method called with double a = "+a);  
    }  
	
	public void inC() {
		System.out.println("In c");
	}
	
	public static void main(String[] args) {
		C c=new  C();
		c.method(10);
		A a=new A();
		a.inA();
	}
}
