
public class Test {

	public static void main(String[] args) {
		
		String s="abc";
		//abc,acb,bac,bca,cab,cba
		check(s, "");
		
		}
	public static void check(String s,String ans) {
		if(s.length()==0) {
			System.out.println(ans);
		}
		
		for(int i=0;i<s.length();i++) {
			char c=s.charAt(i);
			String r=s.substring(0, i)+s.substring(i+1);
			check(r, ans+c);
		}
	}

}
