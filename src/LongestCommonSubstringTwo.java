import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class LongestCommonSubstringTwo {

	public static void main(String[] args) {
		String S1 = "ABC", S2 = "ACB";
		longestCommonSubstr(S1, S2, 0, 0);

	}
	
	public static Set<String> getSub(String s){
		  Set<String> subs=new HashSet<>();
		for(int i=0;i<s.length();i++) {
			for(int j=i;j<s.length();j++) {
				subs.add(s.substring(i,j+1));
			}
		}
		return subs;
	}
	
	static int longestCommonSubstr(String S1, String S2, int n, int m){
      Set<String> sub1=getSub(S1);
      Set<String> sub2=getSub(S2);
     List<String> list= sub1.stream().filter(s->sub2.contains(s)).collect(Collectors.toList());
     Collections.sort(list,(a,b)->Integer.compare(b.length(), a.length()));
     
		System.out.println(list.get(0));
		return 0;
    }

}
