import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class longestUniqueSubsttr {

	public static void main(String[] args) {
		String s="geeksforgeeks";
		String res="";
		Set<String> set=new HashSet<>();
		int i=0;
		int j=0;
		while(i<s.length()) {
			if(!res.contains(Character.toString(s.charAt(i)))) {
				res=res+s.charAt(i);
				if(i==s.length()-1) {
					set.add(res);
				}
				i++;
			}else {
				set.add(res);
				res="";
				j++;
				
				i=j;
			}
			
			
		}
		List<String> list=new ArrayList<>(set);
		
		Collections.sort(list,(a,b)-> Integer.compare(b.length(), a.length()));
		System.out.println(list.get(0));

	}

}
