import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatMethod {

	public static void main(String[] args) {
		Date d=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
		System.out.println(sdf.format(d));
		sdf=new SimpleDateFormat("dd/MM/yyyy");
		System.out.println(sdf.format(d));
		sdf=new SimpleDateFormat("dd MMMM yyyy");
		System.out.println(sdf.format(d));
		sdf=new SimpleDateFormat("dd MMM yyyy");
		System.out.println(sdf.format(d));
		sdf=new SimpleDateFormat("E, dd MMM yyyy");
		System.out.println(sdf.format(d));
		sdf=new SimpleDateFormat("EEEE, dd MMM yyyy");
		System.out.println(sdf.format(d));
	}

}
