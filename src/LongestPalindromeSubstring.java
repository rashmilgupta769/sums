import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class LongestPalindromeSubstring {

	static  Map<String,Integer> resultmap=new LinkedHashMap();
	public static void main(String[] args) {
		String s="ac";
		//System.out.println(getSubsets(s));
		for(String ss:getSubsets(s)) {
			checkPalindrome(ss);
		}
		Map<String,Integer> newMap=resultmap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,(e1,e2)->e1, LinkedHashMap::new));
		for(String ss:newMap.keySet()) {
			System.out.println(ss);
			break;
		}


	}
	
	public static Set<String> getSubsets(String s){
		Set<String> sub=new TreeSet<>();
		
		for(int i=0;i<s.length();i++) {
			for(int j=i;j<s.length();j++) {
				sub.add(s.substring(i,j+1));
			}
		}
		return sub;
		
		
	}
	
	public static void checkPalindrome(String s){
		
		int count=0;
		for(int i=0;i<s.length()/2;i++) {
			if(s.charAt(i)!=s.charAt(s.length()-i-1)) {
				count++;
				break;
			}
		}
		if(count==0) {
			resultmap.put(s, s.length());
		}
		
	}
}
